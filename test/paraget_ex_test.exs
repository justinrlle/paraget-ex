defmodule ParagetExTest do
  use ExUnit.Case
  doctest ParagetEx

  test "ranges are correctly computed" do
    assert ParagetEx.ranges(100, 1) == [[idx: 0, start: 0, end: 99]]

    assert ParagetEx.ranges(100, 5) == [
             [idx: 0, start: 0, end: 19],
             [idx: 1, start: 20, end: 39],
             [idx: 2, start: 40, end: 59],
             [idx: 3, start: 60, end: 79],
             [idx: 4, start: 80, end: 99]
           ]

    assert ParagetEx.ranges(53918, 4) == [
             [idx: 0, start: 0, end: 13478],
             [idx: 1, start: 13479, end: 26957],
             [idx: 2, start: 26958, end: 40436],
             [idx: 3, start: 40437, end: 53917]
           ]
  end
end
