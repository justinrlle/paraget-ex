defmodule ParagetEx do
  require Logger
  import ParagetEx.Macros
  alias ParagetEx.Client

  @spec download(String.t(), integer(), String.t() | nil) :: :ok | {:error, any()}
  def download(url, parts, output \\ nil) do
    with {:ok, content_length} <- head(url) do
      Logger.info("Content-Length of target: #{content_length}")
      send_to = self()
      part_ranges = ranges(content_length, parts)
      Enum.each(part_ranges, &ParagetEx.PartialReq.start_link(send_to, url, &1))
      init_state = Enum.reduce(range(parts), %{}, &Map.put(&2, &1, nil))

      with {:ok, state} <- req_loop(init_state) do
        output = output || file_name(url)
        save_data(state, output, parts)
      end
    end
  end

  @type chunks_state :: %{required(integer()) => binary()}

  @spec req_loop(chunks_state()) :: {:ok, chunks_state()} | {:error, any()}
  def req_loop(state) do
    receive do
      {:finished, idx, chunks} ->
        Logger.debug("Finished downloading part #{idx}: #{byte_size(chunks)} bytes")

        state = Map.put(state, idx, chunks)

        if Enum.all?(Map.keys(state), &Map.fetch!(state, &1)) do
          Logger.info("Finished downloading all data")
          {:ok, state}
        else
          req_loop(state)
        end

      {:get_failure, error} ->
        Logger.error("Error while running partial request: #{error}")
        {:error, error}
    end
  end

  @spec save_data(chunks_state(), String.t(), integer()) :: :ok | {:error, any()}
  def save_data(state, output, parts_count) do
    File.open!(output, [:binary, :write], fn file ->
      Enum.each(range(parts_count), fn idx ->
        chunk = state[idx]
        Logger.debug("Writing chunks from part #{idx} of #{byte_size(chunk)} bytes")
        IO.binwrite(file, chunk)
      end)
    end)
  end

  @spec range(integer()) :: Range.t()
  defp range(parts_count), do: 0..(parts_count - 1)

  @spec file_name(String.t()) :: String.t()
  def file_name(url) do
    path = URI.parse(url).path
    path = if is_nil(path) || path == "/", do: "index.html", else: path

    Path.split(path)
    |> List.last()
  end

  @spec head(String.t()) :: {:ok, integer()} | {:error, any()}
  def head(url) do
    with {:ok, res} <- Client.head(url) do
      case infos_from_headers(res.headers) do
        {content_length, "bytes"} -> {:ok, content_length}
        _ -> {:error, :range_not_supported}
      end
    end
  end

  @spec infos_from_headers(HTTPoison.Request.headers()) :: {integer(), String.t()}
  defp infos_from_headers(headers) do
    with {_, content_length} <- Enum.find(headers, match_header("content-length")),
         {_, accept_ranges} <- Enum.find(headers, match_header("accept-ranges")) do
      {String.to_integer(content_length), accept_ranges}
    end
  end

  @spec ranges(integer(), integer()) :: ParagetEx.PartialReq.range_spec()
  def ranges(content_length, parts_count) do
    part_length = div(content_length, parts_count)

    Enum.map(range(parts_count), fn i ->
      range_start = i * part_length

      range_end =
        if i + 1 == parts_count do
          content_length - 1
        else
          (i + 1) * part_length - 1
        end

      [idx: i, start: range_start, end: range_end]
    end)
  end
end
