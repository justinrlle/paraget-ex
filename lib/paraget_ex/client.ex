defmodule ParagetEx.Client do
  import ParagetEx.Macros

  @user_agent "ParagetEx/v0.1.0"

  @spec head(binary(), HTTPoison.Request.headers(), Keyword.t()) :: {:ok, HTTPoison.AsyncResponse.t()} | {:error, HTTPoison.Error.t()}
  def head(url, headers \\ [], options \\ []) do
    headers = add_ua(headers)
    HTTPoison.head(url, headers, options)
  end

  @spec head(binary(), HTTPoison.Request.headers(), Keyword.t()) :: {:ok, HTTPoison.AsyncResponse.t()} | {:error, HTTPoison.Error.t()}
  def get(url, headers \\ [], options \\ []) do
    headers = add_ua(headers)
    HTTPoison.get(url, headers, options)
  end

  @spec add_ua(HTTPoison.Request.headers()) :: HTTPoison.Request.headers()
  defp add_ua(headers) do
    idx = Enum.find_index(headers, match_header("user-agent"))
    if is_nil(idx) do
      [{"User-Agent", @user_agent} | headers]
    else
      headers
    end
  end
end