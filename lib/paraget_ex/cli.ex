defmodule ParagetEx.Cli do
  @default_opts [parts: 4, output: nil, debug: 0]
  @switches [parts: :integer, output: :string, debug: :count]
  @aliases [o: :output, d: :debug]
  @version Mix.Project.config()[:version]

  @help """
  paraget-ex v#{@version}
  JustinRlle
  Download a file in multiple parts to achieve better download speed.

  USAGE:
    paraget-ex URL [OPTIONS] [FLAGS]  Download from URL.
    paraget-ex -h|--help|-v|--version Show help/version and exit.

  OPTIONS:
    -o, --output OUTPUT Specify the path to the downloaded file. Defaults to the name of the file in the url, or "index.html" if there is none.
    --parts PARTS       Specify the number of parts to download the file. Defaults to 4.

  FLAGS:
    -d, --debug Enable debug logging, pass two times to log more information.
  """

  def main([help]) when help in ~w(-h --help), do: IO.puts(@help)
  def main([version]) when version in ~w(-v --version), do: IO.puts("paraget-ex v#{@version}")

  def main(args) do
    parsed = OptionParser.parse(args, strict: @switches, aliases: @aliases)

    try do
      :ok = run(parsed)
      msg_and_exit("Success", 0)
    rescue
      e in RuntimeError -> msg_and_exit(:stderr, "Error: #{e.message}", 1)
      e -> msg_and_exit(:stderr, "Error: #{inspect(e)}", 2)
    end
  end

  defp msg_and_exit(device \\ :stdio, msg, exit_code) do
    IO.puts(device, msg)
    System.stop(exit_code)
  end


  defp run(args) do
    case args do
      {opts, [url | _], []} ->
        opts = Keyword.merge(@default_opts, opts)
        parts = Keyword.fetch!(opts, :parts)
        output = Keyword.fetch!(opts, :output)
        debug = Keyword.fetch!(opts, :debug)

        level =
          case debug do
            0 -> :warn
            1 -> :info
            x when x >= 2 -> :debug
          end

        Logger.configure(level: level)

        case ParagetEx.download(url, parts, output) do
          {:error, error} -> raise "#{error}"
          _ -> :ok
        end

      {_opts, [], _errors} ->
        raise "You must pass an url to download from."

      {_opts, _argv, [switch | _]} ->
        raise "Invalid option: " <> switch_to_string(switch)
    end
  end

  defp switch_to_string({name, nil}), do: name
  defp switch_to_string({name, val}), do: name <> "=" <> val
end
