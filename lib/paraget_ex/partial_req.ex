defmodule ParagetEx.PartialReq do
  require Logger
  alias ParagetEx.Client

  @type range_spec :: [idx: number, start: number, end: number]

  @spec start_link(pid, String.t(), range_spec) :: {:ok, pid}
  def start_link(send_to, url, range) do
    Task.start_link(ParagetEx.PartialReq, :partial_get, [send_to, url, range])
  end

  @spec partial_get(pid(), String.t(), range_spec()) :: :ok | {:error, any}
  def partial_get(send_to, url, idx: idx, start: start, end: range_end) do
    range = "bytes=#{start}-#{range_end}"
    Logger.info("Will download part ##{idx} with following range: #{range}")
    req = Client.get(url, [{"Range", range}], stream_to: self(), async: :once)

    case req do
      {:ok, req} ->
        handle_stream({send_to, req, idx}, <<>>)

      {:error, error} ->
        send(send_to, {:get_failure, {:http_error, error, idx}})
        {:error, error}
    end
  end

  @type loop_params :: {pid(), HTTPoison.AsyncResponse.t(), integer()}

  @spec next_stream(loop_params(), binary()) :: :ok | {:error, any()}
  defp next_stream({send_to, req, idx}, chunks) do
    with {:ok, req} <- HTTPoison.stream_next(req),
         do: handle_stream({send_to, req, idx}, chunks)
  end

  @spec next_stream(loop_params(), binary()) :: :ok | {:error, any()}
  defp handle_stream({send_to, _, idx} = params, chunks) do
    receive do
      %HTTPoison.AsyncStatus{code: code} ->
        Logger.debug("part ##{idx}: Got AsyncStatus with code #{code}")

        if code >= 200 && code < 300 do
          next_stream(params, chunks)
        else
          send(send_to, {:get_failure, {:http_status, code, idx}})
          {:error, {:http_status, code}}
        end

      %HTTPoison.AsyncHeaders{headers: _headers} ->
        Logger.debug("part ##{idx}: Got AsyncHeaders")
        next_stream(params, chunks)

      %HTTPoison.AsyncChunk{chunk: chunk} ->
        Logger.debug("part ##{idx}: Got AsyncChunk, with a chunk of #{byte_size(chunk)} bytes")
        next_stream(params, chunks <> chunk)

      %HTTPoison.AsyncEnd{} ->
        Logger.debug("part ##{idx}: Got AsyncEnd")
        send(send_to, {:finished, idx, chunks})
        :ok
    end
  end
end
