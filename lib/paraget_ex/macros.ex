defmodule ParagetEx.Macros do
  defmacro match_header(key) do
    regex = Regex.compile!("^#{key}$", "i")
    quote do
      fn {k, _} -> String.match?(k, unquote(Macro.escape(regex))) end
    end
  end
end