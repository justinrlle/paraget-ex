# ParagetEx

Download big files by splitting the download in multiple parts.

## Installation

Clone the repository, and install the app as an `escript`:

```bash
$ git clone https://gitlab.com/justinrlle/paraget-ex.git
$ cd paraget-ex
$ MIX_ENV=prod mix do deps.get, deps.compile, compile, escript.install
```

Now, if you have `$HOME/.mix/escript` in your `$PATH`, you can just run `paraget-ex` from anywhere. You can also use the `paraget.exs` script as an alternative:

```bash
$ mix do deps.get deps.compile, compile
$ mix run ./paragex.exs ARGS
```
