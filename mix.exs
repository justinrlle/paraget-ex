defmodule ParagetEx.MixProject do
  use Mix.Project

  def project do
    [
      app: :paraget_ex,
      version: "0.1.0",
      elixir: "~> 1.9",
      escript: [main_module: ParagetEx.Cli, name: "paraget-ex"],
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  def application do
    [
      applications: [:httpoison],
      extra_applications: [:logger]
    ]
  end

  defp deps do
    [
      {:httpoison, "~> 1.5"},
      {:dialyxir, "~> 1.0.0-rc.6", only: [:dev], runtime: false}
    ]
  end
end
